package com.honeyluck.starwars.common.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class ItemBlueprint extends Item {

    public ItemBlueprint() {
        super(new Properties().group(ItemGroup.MISC));
    }

}
