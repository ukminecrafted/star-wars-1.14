package com.honeyluck.starwars.common.item;

public enum EnumSaberParts {
    HANDLE,
    EMITTER,
    CROSSGUARD,
    POMMEL
}
