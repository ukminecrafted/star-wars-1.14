package com.honeyluck.starwars.common.item;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.common.entity.EntityHolocron;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class ItemHolocron extends Item {

    public ItemHolocron() {
        super(new Properties().group(StarWars.tabItems).maxStackSize(1));

        addPropertyOverride(new ResourceLocation(StarWars.modid, "open"), new IItemPropertyGetter(){

            @Override
            public float call(ItemStack stack, @Nullable World world, @Nullable LivingEntity entity) {
                if(stack.getOrCreateTag().getBoolean("open")) {
                    return 1;
                }
                return 0;
            }
        });
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand) {
        ItemStack stack = player.getHeldItem(hand);
        stack.getOrCreateTag();

        stack.getTag().putBoolean("open", !stack.getTag().getBoolean("open"));
        return new ActionResult<>(ActionResultType.PASS, player.getHeldItem(hand));
    }

    @Override
    public boolean hasCustomEntity(ItemStack stack) {
        if(stack.getOrCreateTag().getBoolean("open")) {
            return true;
        }
        return false;
    }

    @Nullable
    @Override
    public Entity createEntity(World world, Entity location, ItemStack itemstack) {
        EntityHolocron entity = new EntityHolocron(world, location.posX, location.posY + 0.5, location.posZ, itemstack);
        entity.setMotion(location.getMotion());
        return entity;
    }
}
