package com.honeyluck.starwars.common.item;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.client.models.sabers.ModelSaber;
import com.honeyluck.starwars.client.render.RenderSaberPart;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.items.CapabilityItemHandler;

import java.util.concurrent.atomic.AtomicReference;

public class ItemSaberPart extends Item {
    private String model;
    private EnumSaberParts partType;

    public ItemSaberPart(String model, EnumSaberParts partType) {
        super(new Properties().group(StarWars.tabItems).setTEISR(() ->RenderSaberPart::new));
        this.model = model;
        this.partType = partType;
    }

    public String getModel() {
        return model;
    }

    public EnumSaberParts getPartType() {
        return partType;
    }

    @Override
    public ITextComponent getDisplayName(ItemStack stack) {
        AtomicReference<String> name = new AtomicReference<>("");
        if(this.getPartType() == EnumSaberParts.POMMEL) {
            name.set("item." + StarWars.modid + ".lightsaber_pommel.name");
        } else {
            name.set("item." + StarWars.modid + ".lightsaber_emitter.name");
        }
        return new TranslationTextComponent(name.get());
    }
}
