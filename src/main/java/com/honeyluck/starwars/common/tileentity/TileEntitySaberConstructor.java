package com.honeyluck.starwars.common.tileentity;

import com.honeyluck.starwars.client.gui.SaberConstructorScreen;
import com.honeyluck.starwars.common.container.ContainerSaberConstructor;
import com.honeyluck.starwars.common.container.ContainerSaberConstructorModeConstruct;
import com.honeyluck.starwars.common.item.EnumSaberParts;
import com.honeyluck.starwars.common.item.ItemKyberCrystal;
import com.honeyluck.starwars.common.item.ItemSaber;
import com.honeyluck.starwars.common.item.ItemSaberPart;
import com.honeyluck.starwars.events.Registries;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TileEntitySaberConstructor extends TileEntity implements ITickableTileEntity, INamedContainerProvider {

	/* SLOT IDS
	* 0 to 17 - Inventory
	* 18 - Handle/Saber
	* 19 - Emitter
	* 20 - Kyber Crystal
	* 21 - Pommel
	* */
    private ItemStackHandler inventory = new ItemStackHandler(22) {
        @Override
        public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
            if(!stack.isEmpty()) {
                if(slot < 18) {
                    if(stack.getItem() instanceof ItemSaberPart || stack.getItem() instanceof ItemKyberCrystal || stack.getItem() instanceof ItemSaber) {
                        return true;
                    }
                } else {
                    if(slot == 18 && stack.getItem() instanceof ItemSaber) {
                        return true;
                    }
                    if(slot == 19 && stack.getItem() instanceof ItemSaberPart) {
                        ItemSaberPart part = (ItemSaberPart)stack.getItem();
                        if(part.getPartType() == EnumSaberParts.EMITTER || part.getPartType() == EnumSaberParts.CROSSGUARD) {
                            return true;
                        }
                    }
                    if(slot == 20 && stack.getItem() instanceof ItemKyberCrystal) {
                        if(stack.getTag() != null) {
                            return true;
                        }
                    }
                    if(slot == 21 && stack.getItem() instanceof ItemSaberPart) {
                        ItemSaberPart part = (ItemSaberPart)stack.getItem();
                        if(part.getPartType() == EnumSaberParts.POMMEL) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    };

    private SaberConstructorScreen.SaberConstructorMode saberConstructorMode = SaberConstructorScreen.SaberConstructorMode.CONSTRUCT;

    public TileEntitySaberConstructor() {
        super(Registries.saberConstructorType);
    }

    @Override
    public void tick() {
    }

    @Override
    public void read(CompoundNBT tag) {
        CompoundNBT nbt = tag.getCompound("inventory");
        inventory.deserializeNBT(nbt);
	    saberConstructorMode = SaberConstructorScreen.SaberConstructorMode.values()[nbt.getInt("saberConstructorMode")];
        super.read(tag);
    }

    @Override
    public CompoundNBT write(CompoundNBT tag) {
        CompoundNBT nbt = inventory.serializeNBT();
        tag.put("inventory", nbt);
        tag.putInt("saberConstructorMode", saberConstructorMode.ordinal());
        return super.write(tag);
    }

	@Override
	public CompoundNBT getUpdateTag() {
    	CompoundNBT nbt = super.getUpdateTag();
		nbt.putInt("saberConstructorMode", saberConstructorMode.ordinal());
		return nbt;
	}

	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		saberConstructorMode = SaberConstructorScreen.SaberConstructorMode.values()[tag.getInt("saberConstructorMode")];
	}

	@Nullable
	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(getPos(), 1, getUpdateTag());
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.handleUpdateTag(pkt.getNbtCompound());
	}

	@Override
    public ITextComponent getDisplayName() {
	    ResourceLocation regName = getType().getRegistryName();
        return new TranslationTextComponent("container." + regName.getNamespace() + "." + regName.getPath());
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if(cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return LazyOptional.of(() -> (T) this.inventory);
        }
        return super.getCapability(cap, side);
    }

    @Nullable
    @Override
    public Container createMenu(int i, PlayerInventory playerInventory, PlayerEntity playerEntity) {
    	if (saberConstructorMode == SaberConstructorScreen.SaberConstructorMode.MODIFY) {
		    return new ContainerSaberConstructor(i, world, pos, playerInventory);
	    }
	    return new ContainerSaberConstructorModeConstruct(i, world, pos, playerInventory);
    }

    public void setSaberConstructorMode(SaberConstructorScreen.SaberConstructorMode mode) {
    	this.saberConstructorMode = mode;
        this.inventory.getStackInSlot(18).getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(saberHandler -> {
            for(int i = 0; i < 3; i++) {
                if(mode == SaberConstructorScreen.SaberConstructorMode.MODIFY) {
                    ItemStack itemStack = saberHandler.extractItem(i, 1, false);
                    this.inventory.setStackInSlot(19 +  i, itemStack);
                }
                else {
                    ItemStack itemStack = this.inventory.extractItem(19 + i, 1, false);
                    saberHandler.insertItem(i, itemStack, false);
                }
            }
        });
    	this.markDirty();
    	this.getWorld().notifyBlockUpdate(pos, getBlockState(), getBlockState(), 3);

    }

	public SaberConstructorScreen.SaberConstructorMode getSaberConstructorMode() {
		return saberConstructorMode;
	}

	public ItemStackHandler getInventory()
	{
		return inventory;
	}
}
