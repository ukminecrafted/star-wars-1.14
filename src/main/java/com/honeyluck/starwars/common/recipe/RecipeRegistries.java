package com.honeyluck.starwars.common.recipe;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.events.Registries;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

import java.util.ArrayList;
import java.util.List;

@Mod.EventBusSubscriber(modid = StarWars.modid, bus = Mod.EventBusSubscriber.Bus.MOD)
public class RecipeRegistries {

    public static List<ComponentRecipe> componentRecipes = new ArrayList<>();

    @SubscribeEvent
    public static void registerRecipes(FMLCommonSetupEvent event) {

        for(Item item : Registries.STANDARD_HANDLE) {
            componentRecipes.add(new ComponentRecipe(item, Items.IRON_INGOT, Registries.handleBlueprint, Registries.powerCell, Items.IRON_INGOT));
        }

        for(Item item : Registries.STANDARD_EMITTER) {
            componentRecipes.add(new ComponentRecipe(item, Registries.emitterBlueprint, Items.IRON_INGOT, Registries.focusingLens));
        }

        for(Item item : Registries.STANDARD_POMMEL) {
            componentRecipes.add(new ComponentRecipe(item, Registries.pommelBlueprint, Items.IRON_INGOT));
        }

    }
}
