package com.honeyluck.starwars.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IEnviromentBlockReader;

import static net.minecraft.state.properties.BlockStateProperties.FACING;

public class BlockKyberCrystal extends Block {

    public BlockKyberCrystal() {
        super(Properties.create(Material.ROCK));
        this.setDefaultState((BlockState)((BlockState)this.stateContainer.getBaseState()).with(FACING, Direction.UP));
    }

    @Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.TRANSLUCENT;
    }

    public BlockState getStateForPlacement(BlockItemUseContext p_196258_1_) {
        return this.getDefaultState().with(FACING, p_196258_1_.getNearestLookingDirection().getOpposite());
    }

    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(FACING);
    }

    @Override
    public int getLightValue(BlockState state, IEnviromentBlockReader world, BlockPos pos) {
        return 5;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
    	Direction direction = state.get(FACING);
	    IBooleanFunction boolFunc = IBooleanFunction.OR;

    	// base1
	    VoxelShape shape = getShapeForDirection(5, 0, 6, 6, 1, 6, direction);
	    // ground1
	    shape = VoxelShapes.combineAndSimplify(shape, getShapeForDirection(8, 0, 9, 4, 0.5, 4, direction), boolFunc);
	    // ground2
	    shape = VoxelShapes.combineAndSimplify(shape, getShapeForDirection(9, 0, 5, 4, 0.5, 5, direction), boolFunc);
	    // ground3
	    shape = VoxelShapes.combineAndSimplify(shape, getShapeForDirection(5, 0, 4, 4, 0.5, 2, direction), boolFunc);
	    // ground4
	    shape = VoxelShapes.combineAndSimplify(shape, getShapeForDirection(4, 0, 6.5, 4, 0.5, 4, direction), boolFunc);
	    // crystal5
	    shape = VoxelShapes.combineAndSimplify(shape, getShapeForDirection(7, 0, 7, 2.5, 8, 2.5, direction), boolFunc);

        return shape;
    }

    public static VoxelShape getShapeForDirection(double x, double y, double z, double xLength, double height, double zLength, Direction direction) {
    	double factor = 0.0625;

    	// default values use UP facing, normal value
	    double x1 = x;
    	double y1 = y;
	    double z1 = z;
	    double x2 = x+xLength;
	    double y2 = y+height;
	    double z2 = z+zLength;

	    if (direction == Direction.DOWN) {
    		// x: x
		    // y: 16 - y (reversed)
		    // z: 16 - z (reversed)
	    	y1 = 16-y-height;
	    	z1 = 16-z-zLength;
	    	x2 = x+xLength;
	    	y2 = 16-y;
	    	z2 = z1 + zLength;
	    } else if (direction == Direction.NORTH || direction == Direction.SOUTH || direction == Direction.WEST || direction == Direction.EAST) {
		    // y: z

	    	y1 = z;
	    	y2 = z+zLength;

		    if (direction == Direction.NORTH) {
			    // x: x
			    // z: 16 - y (reversed)
			    x1 = x;
			    z1 = 16-y-height;
			    x2 = x+xLength;
			    z2 = 16-y;
		    } else if (direction == Direction.SOUTH) {
			    // x: 16 - x (reversed)
			    // z: y
			    x1 = 16 - x - xLength;
			    z1 = y;
			    x2 = 16 - x;
			    z2 = y + height;
		    } else if (direction == Direction.WEST) {
			    // x: 16 - y (reversed)
			    // z: 16 - x (reversed)
			    x1 = 16 - y - height;
			    z1 = 16 - x;
			    x2 = 16 - y;
			    z2 = 16 - x - xLength;
		    } else { // East
			    // x: y
			    // z: x
			    x1 = y;
			    z1 = x;
			    x2 = y + height;
			    z2 = x + xLength;
		    }
	    }

    	return VoxelShapes.create(x1 * factor, y1 * factor, z1 * factor, x2 * factor, y2 * factor, z2 * factor);
    }
}
