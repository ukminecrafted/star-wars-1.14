package com.honeyluck.starwars.common.block;

import com.honeyluck.starwars.common.tileentity.TileEntityComponentConstructor;
import com.honeyluck.starwars.common.tileentity.TileEntitySaberConstructor;
import com.honeyluck.starwars.events.Registries;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.IProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BedPart;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.items.CapabilityItemHandler;

import javax.annotation.Nullable;

import static net.minecraft.state.properties.BlockStateProperties.HORIZONTAL_FACING;

public class BlockSaberConstructor extends Block {

    public BlockSaberConstructor() {
        super(Properties.create(Material.ROCK).hardnessAndResistance(1.5F, 6.0F));
        this.setDefaultState((BlockState)((BlockState)this.stateContainer.getBaseState()).with(HORIZONTAL_FACING, Direction.NORTH));
    }

    @Override
    public BlockRenderLayer getRenderLayer() {
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if(!worldIn.isRemote) {
            TileEntity te = worldIn.getTileEntity(pos);
            if(te instanceof INamedContainerProvider) {
                NetworkHooks.openGui((ServerPlayerEntity) player, (INamedContainerProvider) te, te.getPos());
            }
        }
        return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        super.onBlockPlacedBy(worldIn, pos, state, placer, stack);
        if (!worldIn.isRemote) {
            BlockPos blockpos = pos.offset(state.get(HORIZONTAL_FACING).rotateY().getOpposite());
            if(worldIn.getBlockState(blockpos).isAir()) {
                worldIn.setBlockState(pos, Registries.componentConstructor.getStateContainer().getBaseState().with(HORIZONTAL_FACING, state.get(HORIZONTAL_FACING)));

                worldIn.setBlockState(blockpos, this.getExtendedState(state, worldIn, blockpos));
                worldIn.notifyNeighbors(blockpos, Blocks.AIR);
                state.updateNeighbors(worldIn, blockpos, 3);
            } else {
                blockpos = pos.offset(state.get(HORIZONTAL_FACING).rotateY());
                if (worldIn.getBlockState(blockpos).isAir()) {
                    worldIn.setBlockState(blockpos, Registries.componentConstructor.getStateContainer().getBaseState().with(HORIZONTAL_FACING, state.get(HORIZONTAL_FACING)));

                    worldIn.notifyNeighbors(pos, Blocks.AIR);
                    state.updateNeighbors(worldIn, pos, 3);
                }
            }

        }
    }

    @Override
    public void onBlockHarvested(World worldIn, BlockPos pos, BlockState state, PlayerEntity player) {
        BlockPos blockpos = pos.offset(state.get(HORIZONTAL_FACING).rotateY());
        worldIn.setBlockState(blockpos, Blocks.AIR.getDefaultState());
        worldIn.notifyNeighbors(blockpos, Blocks.AIR);
        state.updateNeighbors(worldIn, blockpos, 3);
        super.onBlockHarvested(worldIn, pos, state, player);
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.hasTileEntity() && state.getBlock() != newState.getBlock()) {
            // drops everything in the inventory
            worldIn.getTileEntity(pos).getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(h -> {
                for (int i = 0; i < h.getSlots(); i++) {
                    spawnAsEntity(worldIn, pos, h.getStackInSlot(i));
                }
            });
            worldIn.removeTileEntity(pos);
        }
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new TileEntitySaberConstructor();
    }

    public BlockState getStateForPlacement(BlockItemUseContext p_196258_1_) {
        return (BlockState)this.getDefaultState().with(HORIZONTAL_FACING, p_196258_1_.getPlacementHorizontalFacing().getOpposite());
    }
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(new IProperty[]{HORIZONTAL_FACING});
    }
    
}
