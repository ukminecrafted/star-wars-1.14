package com.honeyluck.starwars.common.utils;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.Vec3d;

import java.util.Random;

public class SaberColours {

    public static Vec3d red = new Vec3d(255, 0,0);
    public static Vec3d orange = new Vec3d(255, 100, 0);
    public static Vec3d green = new Vec3d(50, 255, 100);
    public static Vec3d lightGreen = new Vec3d(100, 255, 0);
    public static Vec3d blue = new Vec3d(50, 100, 255);
    public static Vec3d yellow = new Vec3d(250, 230, 0);
    public static Vec3d purple = new Vec3d(100, 0, 180);
    public static Vec3d magenta = new Vec3d(255, 0, 255);
    public static Vec3d initial = new Vec3d(250, 250, 250);

    public static Vec3d[] jediColours = new Vec3d[]{green, lightGreen, blue, yellow};
    public static Vec3d[] neutralColours = new Vec3d[]{yellow, purple};
    public static Vec3d[] sithColours = new Vec3d[]{red, orange};
    public static Vec3d[] allColours = new Vec3d[]{green, lightGreen, blue, yellow, purple, magenta, red, orange};

    private static Random random = new Random();

    public static Vec3d getRandom(Vec3d[] array) {
        for(int i = 0; i < array.length; i ++) {
            random.nextInt(); //Generate some random numbers so that the value used for the random number isn't the same when first generating
        }
        int value = random.nextInt(array.length);
        return array[value];
    }

}
