package com.honeyluck.starwars.common.entity;

import com.honeyluck.starwars.events.Registries;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.Pose;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;

public class EntityHolocron extends Entity {

    private static final DataParameter<ItemStack> ITEM = EntityDataManager.createKey(EntityHolocron.class, DataSerializers.ITEMSTACK);

    public EntityHolocron(EntityType<?> entityTypeIn, World worldIn) {
        super(entityTypeIn, worldIn);
    }

    public EntityHolocron(World world, double posX, double posY, double posZ, ItemStack itemstack) {
        this(Registries.holocronEntityType, world);
        this.setPosition(posX, posY, posZ);
        this.setItem(itemstack);
    }

    @Override
    public ActionResultType applyPlayerInteraction(PlayerEntity player, Vec3d vec, Hand hand) {
        if(player.getHeldItem(hand).isEmpty()) {
            player.setHeldItem(hand, this.getItem());
            this.remove();
            return ActionResultType.SUCCESS;
        }
        return super.applyPlayerInteraction(player, vec, hand);
    }

    @Override
    public boolean canBeCollidedWith() {
        return true;
    }

    @Override
    public EntitySize getSize(Pose poseIn) {
        return Registries.holocronEntityType.getSize();
    }

    public ItemStack getItem() {
        return this.getDataManager().get(ITEM);
    }

    public void setItem(ItemStack stack) {
        this.getDataManager().set(ITEM, stack);
    }

    @Override
    protected void registerData() {
        this.dataManager.register(ITEM, ItemStack.EMPTY);
    }

    @Override
    protected void readAdditional(CompoundNBT compound) {
        CompoundNBT nbt = compound.getCompound("Item");
        this.setItem(ItemStack.read(nbt));
        if(this.getItem().isEmpty()) {
            this.remove();
        }
    }

    @Override
    protected void writeAdditional(CompoundNBT compound) {
        if(!this.getItem().isEmpty()) {
            compound.put("Item", this.getItem().write(new CompoundNBT()));
        }
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }
}
