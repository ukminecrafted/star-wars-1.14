package com.honeyluck.starwars.client.models.sabers;

import com.honeyluck.starwars.StarWars;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;

public class CrossSaber extends ModelSaber {
	private final RendererModel lightsaber;
	private final RendererModel endcap;
	private final RendererModel handle;
	private final RendererModel rings;
	private final RendererModel buttons;
	private final RendererModel emitter;
	private final RendererModel vents;
	private final RendererModel ventw;
	private final RendererModel vente;
	private final RendererModel emitterRing;
	public ResourceLocation TEX = new ResourceLocation(StarWars.modid, "textures/items/cross_saber.png");

	public CrossSaber() {
		textureWidth = 64;
		textureHeight = 64;

		lightsaber = new RendererModel(this);
		lightsaber.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		endcap = new RendererModel(this);
		endcap.setRotationPoint(0.0F, -0.125F, 0.0F);
		lightsaber.addChild(endcap);
		endcap.cubeList.add(new ModelBox(endcap, 0, 33, -2.0F, 1.875F, -2.0F, 4, 1, 4, 0.0F, false));
		endcap.cubeList.add(new ModelBox(endcap, 24, 18, -2.0F, 3.625F, -2.0F, 4, 1, 4, 0.0F, false));
		endcap.cubeList.add(new ModelBox(endcap, 12, 13, -2.0F, 0.0F, -2.0F, 4, 5, 4, -0.125F, false));

		handle = new RendererModel(this);
		handle.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(handle);
		handle.cubeList.add(new ModelBox(handle, 0, 17, -1.5F, -9.0F, -1.5F, 3, 8, 3, 0.0F, false));
		handle.cubeList.add(new ModelBox(handle, 16, 6, -2.0F, -2.875F, -2.0F, 4, 3, 4, -0.125F, false));

		rings = new RendererModel(this);
		rings.setRotationPoint(2.0F, -6.9F, -2.0F);
		handle.addChild(rings);
		rings.cubeList.add(new ModelBox(rings, 32, 32, -4.0F, -2.0F, 0.0F, 4, 1, 4, -0.125F, false));
		rings.cubeList.add(new ModelBox(rings, 20, 29, -4.0F, -1.0F, 0.0F, 4, 1, 4, -0.125F, false));
		rings.cubeList.add(new ModelBox(rings, 28, 10, -4.0F, 0.0F, 0.0F, 4, 1, 4, -0.125F, false));
		rings.cubeList.add(new ModelBox(rings, 28, 5, -4.0F, 1.0F, 0.0F, 4, 1, 4, -0.125F, false));
		rings.cubeList.add(new ModelBox(rings, 8, 28, -4.0F, 2.0F, 0.0F, 4, 1, 4, -0.125F, false));
		rings.cubeList.add(new ModelBox(rings, 26, 0, -4.0F, 3.0F, 0.0F, 4, 1, 4, -0.125F, false));

		buttons = new RendererModel(this);
		buttons.setRotationPoint(0.0F, 0.0F, 0.0F);
		handle.addChild(buttons);
		buttons.cubeList.add(new ModelBox(buttons, 0, 8, -0.5F, -2.5F, -2.1F, 1, 1, 1, -0.1F, false));
		buttons.cubeList.add(new ModelBox(buttons, 0, 6, -0.5F, -1.25F, -2.1F, 1, 1, 1, -0.1F, false));

		emitter = new RendererModel(this);
		emitter.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(emitter);
		emitter.cubeList.add(new ModelBox(emitter, 0, 6, -2.0F, -16.0F, -2.0F, 4, 7, 4, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 12, 22, -2.0F, -13.25F, -2.0F, 4, 2, 4, 0.125F, false));
		emitter.cubeList.add(new ModelBox(emitter, 24, 24, -2.0F, -10.5F, -2.0F, 4, 1, 4, 0.125F, false));
		emitter.cubeList.add(new ModelBox(emitter, 16, 34, -1.5F, -16.35F, -1.5F, 3, 1, 3, 0.0F, false));

		vents = new RendererModel(this);
		vents.setRotationPoint(3.0F, 0.0F, 0.5F);
		emitter.addChild(vents);
		vents.cubeList.add(new ModelBox(vents, 0, 0, -8.0F, -15.5F, -2.0F, 10, 3, 3, 0.0F, false));

		ventw = new RendererModel(this);
		ventw.setRotationPoint(0.0F, 0.0F, 0.0F);
		vents.addChild(ventw);
		ventw.cubeList.add(new ModelBox(ventw, 24, 0, 2.25F, -12.5F, -1.5F, 0, 0, 2, 0.25F, false));
		ventw.cubeList.add(new ModelBox(ventw, 24, 0, 2.25F, -15.5F, -1.5F, 0, 0, 2, 0.25F, false));
		ventw.cubeList.add(new ModelBox(ventw, 24, 0, 2.25F, -15.5F, 1.0F, 0, 3, 0, 0.25F, false));
		ventw.cubeList.add(new ModelBox(ventw, 24, 0, 2.25F, -15.5F, -2.0F, 0, 3, 0, 0.25F, false));

		vente = new RendererModel(this);
		vente.setRotationPoint(-6.0F, 0.0F, 0.0F);
		vents.addChild(vente);
		vente.cubeList.add(new ModelBox(vente, 24, 0, -2.25F, -12.5F, -1.5F, 0, 0, 2, 0.25F, false));
		vente.cubeList.add(new ModelBox(vente, 24, 0, -2.25F, -15.5F, -1.5F, 0, 0, 2, 0.25F, false));
		vente.cubeList.add(new ModelBox(vente, 24, 0, -2.25F, -15.5F, -2.0F, 0, 3, 0, 0.25F, false));
		vente.cubeList.add(new ModelBox(vente, 24, 0, -2.25F, -15.5F, 1.0F, 0, 3, 0, 0.25F, false));

		emitterRing = new RendererModel(this);
		emitterRing.setRotationPoint(0.0F, 0.0F, 0.0F);
		emitter.addChild(emitterRing);
		emitterRing.cubeList.add(new ModelBox(emitterRing, 36, 15, 1.0F, -17.0F, -2.0F, 1, 1, 4, -0.25F, false));
		emitterRing.cubeList.add(new ModelBox(emitterRing, 24, 34, -2.0F, -17.0F, -2.0F, 1, 1, 4, -0.25F, false));
		emitterRing.cubeList.add(new ModelBox(emitterRing, 12, 8, -1.5F, -17.0F, -2.0F, 3, 1, 1, -0.25F, false));
		emitterRing.cubeList.add(new ModelBox(emitterRing, 12, 6, -1.5F, -17.0F, 1.0F, 3, 1, 1, -0.25F, false));
	}

	@Override
	public void renderHandle(float f5) {
		bindTex();
		handle.render(f5);
	}
	@Override
	public void renderEmitter(float f5) {
		bindTex();
		emitter.render(f5);
	}
	@Override
	public void renderPommel(float f5) {
		bindTex();
		endcap.render(f5);
	}
	@Override
	public void render(float f5) {
		bindTex();
		lightsaber.render(f5);
	}
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
	@Override
	public void bindTex() {
		mc.getTextureManager().bindTexture(TEX);
	}

	@Override
	public float getHeight() {
		return 9f;
	}

	@Override
	public float getEmitterHeight() {
		return 7.75f;
	}

	@Override
	public float getCrossGuardOffset() {
		return 2.75f;
	}
}