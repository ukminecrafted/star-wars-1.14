package com.honeyluck.starwars.client.models.sabers;//Made with Blockbench by dhi_awesome
//Paste this code into your mod.

import com.honeyluck.starwars.StarWars;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class SkywalkerSaber extends ModelSaber {
	public RendererModel lightsaber;
	private RendererModel endcap;
	private RendererModel blackbars;
	private RendererModel handle;
	private RendererModel sideswitch;
	private RendererModel switchbump;
	private RendererModel button;
	private RendererModel dots;
	private RendererModel releaseknob;
	private RendererModel emitter;
	private RendererModel clip;
	public ResourceLocation TEX = new ResourceLocation(StarWars.modid, "textures/items/skywalker_saber.png");

	public SkywalkerSaber() {
		textureWidth = 64;
		textureHeight = 64;

		lightsaber = new RendererModel(this);
		lightsaber.setRotationPoint(0.0F, 24.0F, 0.0F);

		endcap = new RendererModel(this);
		endcap.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(endcap);
		endcap.cubeList.add(new ModelBox(endcap, 12, 12, -2.0F, -0.5F, -2.0F, 4, 9, 4, 0.0F, false));

		blackbars = new RendererModel(this);
		blackbars.setRotationPoint(0.0F, 5.5F, 0.0F);
		setRotationAngle(blackbars, 0.0F, -0.7854F, 0.0F);
		endcap.addChild(blackbars);
		blackbars.cubeList.add(new ModelBox(blackbars, 16, 0, -2.5F, -4.5F, -1.0F, 5, 7, 2, 0.0F, false));
		blackbars.cubeList.add(new ModelBox(blackbars, 0, 22, -1.0F, -4.5F, -2.5F, 2, 7, 5, 0.0F, false));

		handle = new RendererModel(this);
		handle.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(handle);
		handle.cubeList.add(new ModelBox(handle, 24, 11, -2.0F, -1.0F, -2.0F, 4, 1, 4, 0.125F, false));
		handle.cubeList.add(new ModelBox(handle, 28, 16, -2.0F, -4.0F, -2.0F, 4, 1, 4, 0.125F, false));
		handle.cubeList.add(new ModelBox(handle, 0, 0, -2.0F, -12.5F, -2.0F, 4, 12, 4, 0.0F, false));

		sideswitch = new RendererModel(this);
		sideswitch.setRotationPoint(0.0F, 0.0F, 0.0F);
		handle.addChild(sideswitch);
		sideswitch.cubeList.add(new ModelBox(sideswitch, 30, 4, -3.0F, -4.0F, -1.0F, 1, 4, 2, 0.0F, false));
		sideswitch.cubeList.add(new ModelBox(sideswitch, 12, 0, -3.0625F, -2.8125F, -1.5F, 1, 2, 1, -0.1875F, false));

		switchbump = new RendererModel(this);
		switchbump.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(switchbump, 0.1745F, 0.0F, 0.0F);
		sideswitch.addChild(switchbump);
		switchbump.cubeList.add(new ModelBox(switchbump, 0, 0, -3.0625F, -3.8248F, -1.0966F, 1, 2, 1, -0.1875F, false));

		button = new RendererModel(this);
		button.setRotationPoint(0.0F, 0.0F, 0.125F);
		handle.addChild(button);
		button.cubeList.add(new ModelBox(button, 5, 16, -1.0F, -11.3125F, -2.4375F, 2, 2, 1, 0.0F, false));
		button.cubeList.add(new ModelBox(button, 28, 21, -1.0F, -11.3125F, -2.9375F, 2, 2, 1, -0.125F, false));

		dots = new RendererModel(this);
		dots.setRotationPoint(1.0F, 0.0F, 0.125F);
		setRotationAngle(dots, -0.7854F, 0.0F, 0.0F);
		handle.addChild(dots);
		dots.cubeList.add(new ModelBox(dots, 8, 19, 0.125F, -7.8042F, -10.525F, 1, 1, 1, -0.25F, false));
		dots.cubeList.add(new ModelBox(dots, 24, 11, -3.125F, -7.8042F, -10.525F, 1, 1, 1, -0.25F, false));
		dots.cubeList.add(new ModelBox(dots, 0, 21, 0.125F, -7.4506F, -10.1715F, 1, 1, 1, -0.25F, false));
		dots.cubeList.add(new ModelBox(dots, 0, 23, -3.125F, -7.4506F, -10.1715F, 1, 1, 1, -0.25F, false));

		releaseknob = new RendererModel(this);
		releaseknob.setRotationPoint(0.0F, 0.0F, 0.0F);
		handle.addChild(releaseknob);
		releaseknob.cubeList.add(new ModelBox(releaseknob, 30, 0, -1.0F, -12.5F, 1.375F, 2, 2, 2, -0.375F, false));
		releaseknob.cubeList.add(new ModelBox(releaseknob, 34, 21, -1.0F, -12.5F, 2.0625F, 2, 2, 1, -0.25F, false));

		emitter = new RendererModel(this);
		emitter.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(emitter);
		emitter.cubeList.add(new ModelBox(emitter, 24, 24, -2.0F, -15.5F, -2.0F, 4, 3, 4, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 0, 16, 1.25F, -16.625F, -0.75F, 1, 2, 3, -0.25F, false));
		emitter.cubeList.add(new ModelBox(emitter, 14, 27, -2.0F, -17.0F, 1.25F, 4, 2, 1, -0.25F, false));
		emitter.cubeList.add(new ModelBox(emitter, 14, 30, -2.25F, -16.625F, -0.75F, 1, 2, 3, -0.25F, false));

		clip = new RendererModel(this);
		clip.setRotationPoint(0.0F, 0.0F, 0.0F);
		emitter.addChild(clip);
		clip.cubeList.add(new ModelBox(clip, 22, 31, -1.0F, -16.25F, 1.75F, 2, 4, 1, -0.2375F, false));
		clip.cubeList.add(new ModelBox(clip, 28, 31, -1.0F, -16.125F, 2.25F, 2, 3, 1, -0.25F, false));
		clip.cubeList.add(new ModelBox(clip, 12, 35, 0.125F, -16.25F, 2.125F, 1, 2, 2, -0.375F, false));
		clip.cubeList.add(new ModelBox(clip, 32, 33, -1.125F, -16.25F, 2.125F, 1, 2, 2, -0.375F, false));
	}
	@Override
	public void renderHandle(float f5) {
		bindTex();
		handle.render(f5);
	}
	@Override
	public void renderEmitter(float f5) {
		bindTex();
		emitter.render(f5);
	}
	@Override
	public void renderPommel(float f5) {
		bindTex();
		endcap.render(f5);
	}
	@Override
	public void render(float f5) {
		bindTex();
		lightsaber.render(f5);
	}
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
	@Override
	public void bindTex() {
		mc.getTextureManager().bindTexture(TEX);
	}

	@Override
	public float getHeight() {
		return 12.5f;
	}

	@Override
	public float getEmitterHeight() {
		return 2.5f;
	}
}