package com.honeyluck.starwars.client.models.sabers;//Made with Blockbench
//Paste this code into your mod.

import com.honeyluck.starwars.StarWars;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class WinduSaber extends ModelSaber {
	private final RendererModel lightsaber;
	private final RendererModel endcap;
	private final RendererModel emitter;
	private final RendererModel handle;
	private final RendererModel bars;
	public ResourceLocation TEX = new ResourceLocation(StarWars.modid, "textures/items/windu_saber.png");

	public WinduSaber() {
		textureWidth = 64;
		textureHeight = 64;

		lightsaber = new RendererModel(this);
		lightsaber.setRotationPoint(0.0F, 24.0F, 0.0F);

		endcap = new RendererModel(this);
		endcap.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(endcap);
		endcap.cubeList.add(new ModelBox(endcap, 17, 0, -2.0F, 0.6F, -2.0F, 4, 2, 4, 0.0F, false));
		endcap.cubeList.add(new ModelBox(endcap, 0, 22, 1.3F, 1.0F, -0.5F, 1, 1, 1, -0.125F, false));
		endcap.cubeList.add(new ModelBox(endcap, 5, 22, 2.1F, 1.0F, -0.5F, 1, 1, 1, 0.0F, false));
		endcap.cubeList.add(new ModelBox(endcap, 17, 7, -2.0F, 3.85F, -2.0F, 4, 1, 4, 0.0F, false));
		endcap.cubeList.add(new ModelBox(endcap, 18, 14, -1.0F, 5.55F, -1.0F, 2, 1, 2, 0.0F, false));
		endcap.cubeList.add(new ModelBox(endcap, 0, 0, -2.0F, -0.125F, -2.0F, 4, 1, 4, -0.125F, false));
		endcap.cubeList.add(new ModelBox(endcap, 0, 7, -2.0F, 2.075F, -2.0F, 4, 2, 4, -0.125F, false));
		endcap.cubeList.add(new ModelBox(endcap, 0, 14, -2.0F, 4.575F, -2.0F, 4, 2, 4, -0.125F, false));

		emitter = new RendererModel(this);
		emitter.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(emitter);
		emitter.cubeList.add(new ModelBox(emitter, 0, 33, -2.0F, -12.75F, -2.0F, 4, 1, 4, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 0, 40, -2.0F, -16.625F, -2.0F, 4, 4, 4, -0.125F, false));
		emitter.cubeList.add(new ModelBox(emitter, 0, 40, -2.0F, -16.625F, -2.0F, 4, 1, 4, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 34, 15, -2.0F, -16.625F, -1.0F, 4, 4, 3, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 0, 49, -1.5F, -14.925F, -2.1F, 1, 2, 1, -0.125F, false));
		emitter.cubeList.add(new ModelBox(emitter, 0, 49, -0.5F, -14.925F, -2.1F, 1, 2, 1, -0.125F, false));
		emitter.cubeList.add(new ModelBox(emitter, 0, 55, -1.5F, -15.825F, -2.1F, 3, 1, 1, -0.125F, false));
		emitter.cubeList.add(new ModelBox(emitter, 0, 49, 0.5F, -14.925F, -2.1F, 1, 2, 1, -0.125F, false));
		emitter.cubeList.add(new ModelBox(emitter, 18, 14, -1.0F, -16.85F, -1.0F, 2, 1, 2, 0.0F, false));

		handle = new RendererModel(this);
		handle.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(handle);
		handle.cubeList.add(new ModelBox(handle, 34, 0, -2.0F, -9.0F, -2.0F, 4, 9, 4, 0.0F, false));
		handle.cubeList.add(new ModelBox(handle, 0, 25, -2.0F, -11.875F, -2.0F, 4, 3, 4, -0.125F, false));
		handle.cubeList.add(new ModelBox(handle, 0, 25, -2.0F, -10.125F, -2.0F, 4, 1, 4, 0.0F, false));
		handle.cubeList.add(new ModelBox(handle, 0, 25, -2.0F, -11.375F, -2.0F, 4, 1, 4, 0.0F, false));

		bars = new RendererModel(this);
		bars.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bars, 0.0F, 0.7854F, 0.0F);
		handle.addChild(bars);
		bars.cubeList.add(new ModelBox(bars, 52, 0, -1.0F, -8.5F, -2.5F, 2, 8, 2, 0.0F, false));
		bars.cubeList.add(new ModelBox(bars, 52, 0, -1.0F, -8.5F, 0.5F, 2, 8, 2, 0.0F, false));
		bars.cubeList.add(new ModelBox(bars, 52, 0, -2.5F, -8.5F, -1.0F, 2, 8, 2, 0.0F, false));
		bars.cubeList.add(new ModelBox(bars, 52, 0, 0.5F, -8.5F, -1.0F, 2, 8, 2, 0.0F, false));
	}

	@Override
	public void renderHandle(float f5) {
		bindTex();
		handle.render(f5);
	}

	@Override
	public void renderEmitter(float f5) {
		bindTex();
		emitter.render(f5);
	}

	@Override
	public void renderPommel(float f5) {
		bindTex();
		endcap.render(f5);
	}

	@Override
	public void render(float f5) {
		bindTex();
		lightsaber.render(f5);
	}

	@Override
	public void bindTex() {
		mc.getTextureManager().bindTexture(TEX);
	}

	@Override
	public float getHeight() {
		return 11.75f;
	}

	@Override
	public float getEmitterHeight() {
		return 5f;
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}