package com.honeyluck.starwars.client.models.sabers;// Made with Blockbench
// Paste this code into your mod.
// Make sure to generate all required imports


import com.honeyluck.starwars.StarWars;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;

public class KenobiSaber extends ModelSaber {
	private final RendererModel lightsaber;
	private final RendererModel pommel;
	private final RendererModel notch1;
	private final RendererModel notch2;
	private final RendererModel handle;
	private final RendererModel segment3;
	private final RendererModel emitter;
	public ResourceLocation TEX = new ResourceLocation(StarWars.modid, "textures/items/kenobi_saber.png");

	public KenobiSaber() {
		textureWidth = 64;
		textureHeight = 64;

		lightsaber = new RendererModel(this);
		lightsaber.setRotationPoint(0.0F, 24.0F, 0.0F);

		pommel = new RendererModel(this);
		pommel.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(pommel);
		pommel.cubeList.add(new ModelBox(pommel, 12, 39, -2.0F, 4.5F, -2.0F, 4, 3, 4, 0.0F, false));
		pommel.cubeList.add(new ModelBox(pommel, 38, 18, -2.0F, 0.1F, -2.0F, 4, 4, 4, 0.1F, false));
		pommel.cubeList.add(new ModelBox(pommel, 19, 46, -1.5F, 4.0F, -1.5F, 3, 1, 3, 0.0F, false));

		notch1 = new RendererModel(this);
		notch1.setRotationPoint(0.0F, 4.5F, 0.0F);
		setRotationAngle(notch1, 0.0F, -0.3491F, 0.0F);
		pommel.addChild(notch1);
		notch1.cubeList.add(new ModelBox(notch1, 28, 41, -2.0F, 0.5F, -2.0F, 4, 2, 4, 0.0F, false));

		notch2 = new RendererModel(this);
		notch2.setRotationPoint(0.0F, 5.0F, 0.0F);
		setRotationAngle(notch2, 0.0F, 0.3491F, 0.0F);
		pommel.addChild(notch2);
		notch2.cubeList.add(new ModelBox(notch2, 38, 26, -2.0F, 0.0F, -2.0F, 4, 2, 4, 0.0F, false));

		handle = new RendererModel(this);
		handle.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(handle);
		handle.cubeList.add(new ModelBox(handle, 0, 27, -1.5F, -16.0F, -1.5F, 3, 16, 3, 0.0F, false));
		handle.cubeList.add(new ModelBox(handle, 38, 8, -2.0F, -6.0F, -2.0F, 4, 6, 4, 0.0F, false));
		handle.cubeList.add(new ModelBox(handle, 39, 49, 1.5F, -5.5F, -1.0F, 1, 6, 2, 0.0F, false));
		handle.cubeList.add(new ModelBox(handle, 0, 0, -1.0F, -3.5F, -2.5F, 2, 2, 1, 0.0F, false));

		segment3 = new RendererModel(this);
		segment3.setRotationPoint(0.0F, 0.0F, 0.0F);
		handle.addChild(segment3);
		segment3.cubeList.add(new ModelBox(segment3, 18, 21, -3.0F, -8.5F, -3.0F, 6, 3, 6, -1.0F, false));
		segment3.cubeList.add(new ModelBox(segment3, 18, 12, -3.0F, -10.0F, -3.0F, 6, 3, 6, -1.0F, false));
		segment3.cubeList.add(new ModelBox(segment3, 18, 3, -3.0F, -11.5F, -3.0F, 6, 3, 6, -1.0F, false));
		segment3.cubeList.add(new ModelBox(segment3, 0, 18, -3.0F, -13.0F, -3.0F, 6, 3, 6, -1.0F, false));
		segment3.cubeList.add(new ModelBox(segment3, 0, 9, -3.0F, -14.5F, -3.0F, 6, 3, 6, -1.0F, false));
		segment3.cubeList.add(new ModelBox(segment3, 0, 0, -3.0F, -16.0F, -3.0F, 6, 3, 6, -1.0F, false));

		emitter = new RendererModel(this);
		emitter.setRotationPoint(0.0F, 5.0F, 0.0F);
		lightsaber.addChild(emitter);
		emitter.cubeList.add(new ModelBox(emitter, 31, 47, -1.0F, -27.0F, -1.0F, 2, 6, 2, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 36, 0, -2.5F, -24.0F, -2.5F, 5, 3, 5, -1.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 32, 32, -2.5F, -26.25F, -2.5F, 5, 4, 5, -1.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 12, 30, -2.5F, -29.25F, -2.5F, 5, 4, 5, -1.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 40, 43, -2.0F, -29.0F, -2.0F, 4, 2, 4, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 29, 1, -2.5F, -30.0F, 1.5F, 5, 1, 1, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 18, 0, -2.5F, -30.0F, -2.5F, 5, 1, 1, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 12, 46, 1.5F, -30.0F, -2.5F, 1, 1, 5, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 0, 46, -2.5F, -30.0F, -2.5F, 1, 1, 5, 0.0F, false));
	}
	@Override
	public void renderHandle(float f5) {
		bindTex();
		handle.render(f5);
	}

	@Override
	public void renderEmitter(float f5) {
		bindTex();
		emitter.render(f5);
	}

	@Override
	public void renderPommel(float f5) {
		bindTex();
		pommel.render(f5);
	}

	@Override
	public void render(float f5) {
		bindTex();
		lightsaber.render(f5);
	}

	@Override
	public void bindTex() {
		mc.getTextureManager().bindTexture(TEX);
	}

	@Override
	public float getHeight() {
		return 16f;
	}

	@Override
	public float getEmitterHeight() {
		return 9f;
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}