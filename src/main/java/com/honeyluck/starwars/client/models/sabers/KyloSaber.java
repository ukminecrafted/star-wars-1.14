package com.honeyluck.starwars.client.models.sabers;

import com.honeyluck.starwars.StarWars;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;

public class KyloSaber extends ModelSaber {
	public RendererModel lightsaber;
	public RendererModel endcap;
	public RendererModel bars;
	public RendererModel endbump;
	public RendererModel handle;
	public RendererModel handlebump;
	public RendererModel wire;
	public RendererModel emitter;
	public RendererModel vents;
	public RendererModel ventw;
	public RendererModel vente;
	public RendererModel button;
	public ResourceLocation TEX = new ResourceLocation(StarWars.modid, "textures/items/kylo_saber.png");

	public KyloSaber() {
		textureWidth = 64;
		textureHeight = 64;

		lightsaber = new RendererModel(this);
		lightsaber.setRotationPoint(0.0F, 24.0F, 0.0F);

		endcap = new RendererModel(this);
		endcap.setRotationPoint(0.0F, -0.125F, 0.0F);
		lightsaber.addChild(endcap);
		endcap.cubeList.add(new ModelBox(endcap, 24, 24, -2.0F, 0.875F, -2.0F, 4, 4, 4, 0.0F, false));
		endcap.cubeList.add(new ModelBox(endcap, 43, 24, -2.0F, 4.5F, -2.0F, 4, 1, 4, 0.0F, false));
		endcap.cubeList.add(new ModelBox(endcap, 10, 31, -2.0F, 0.0F, -2.0F, 4, 1, 4, -0.125F, false));
		endcap.cubeList.add(new ModelBox(endcap, 8, 6, -2.0F, 5.2461F, -2.0F, 4, 0, 4, 0.25F, false));
		endcap.cubeList.add(new ModelBox(endcap, 17, 25, -0.5F, -0.75F, 1.5F, 1, 5, 1, 0.0F, false));

		bars = new RendererModel(this);
		bars.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bars, 0.0F, -0.7854F, 0.0F);
		endcap.addChild(bars);
		bars.cubeList.add(new ModelBox(bars, 26, 32, -2.5F, 1.125F, -1.0F, 5, 4, 2, 0.0F, false));
		bars.cubeList.add(new ModelBox(bars, 24, 13, -3.0F, 4.5938F, -1.5F, 6, 1, 3, -0.375F, false));
		bars.cubeList.add(new ModelBox(bars, 26, 0, -3.0F, 4.3438F, -1.5F, 6, 1, 3, -0.375F, false));
		bars.cubeList.add(new ModelBox(bars, 0, 26, -1.0F, 1.125F, -2.5F, 2, 4, 5, 0.0F, false));
		bars.cubeList.add(new ModelBox(bars, 0, 19, -1.5F, 4.5938F, -3.0F, 3, 1, 6, -0.375F, false));
		bars.cubeList.add(new ModelBox(bars, 16, 6, -1.5F, 4.3438F, -3.0F, 3, 1, 6, -0.375F, false));

		endbump = new RendererModel(this);
		endbump.setRotationPoint(0.0F, 0.125F, 0.0F);
		endcap.addChild(endbump);
		endbump.cubeList.add(new ModelBox(endbump, 9, 26, -0.5F, 0.875F, -2.375F, 1, 3, 1, -0.1875F, false));
		endbump.cubeList.add(new ModelBox(endbump, 10, 36, 1.375F, 0.875F, -2.0F, 1, 3, 4, -0.25F, false));
		endbump.cubeList.add(new ModelBox(endbump, 36, 36, -2.375F, 0.875F, -2.0F, 1, 3, 4, -0.25F, false));

		handle = new RendererModel(this);
		handle.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(handle);
		handle.cubeList.add(new ModelBox(handle, 0, 6, -2.0F, -9.0F, -2.0F, 4, 9, 4, 0.0F, false));
		handle.cubeList.add(new ModelBox(handle, 28, 17, -2.0F, -9.375F, -2.0F, 4, 1, 4, -0.125F, false));

		handlebump = new RendererModel(this);
		handlebump.setRotationPoint(0.0F, -11.0F, 0.0F);
		handle.addChild(handlebump);
		handlebump.cubeList.add(new ModelBox(handlebump, 13, 26, -0.5F, 1.875F, 1.375F, 1, 3, 1, -0.1875F, false));
		handlebump.cubeList.add(new ModelBox(handlebump, 0, 6, -0.5F, 1.875F, -2.375F, 1, 3, 1, -0.1875F, true));
		handlebump.cubeList.add(new ModelBox(handlebump, 0, 35, 1.375F, 1.875F, -2.0F, 1, 3, 4, -0.25F, false));
		handlebump.cubeList.add(new ModelBox(handlebump, 20, 36, -2.375F, 1.875F, -2.0F, 1, 3, 4, -0.25F, false));

		wire = new RendererModel(this);
		wire.setRotationPoint(0.0F, 0.0F, 0.0F);
		handle.addChild(wire);
		wire.cubeList.add(new ModelBox(wire, 30, 38, 0.6875F, -9.5F, 1.625F, 1, 10, 1, -0.25F, false));
		wire.cubeList.add(new ModelBox(wire, 0, 26, 0.6875F, -12.5F, 1.875F, 1, 4, 1, -0.25F, false));
		wire.cubeList.add(new ModelBox(wire, 28, 4, 1.1875F, -0.75F, 1.5F, 1, 3, 1, -0.25F, false));

		emitter = new RendererModel(this);
		emitter.setRotationPoint(0.0F, 0.0F, 0.0F);
		lightsaber.addChild(emitter);
		emitter.cubeList.add(new ModelBox(emitter, 12, 15, -2.0F, -15.25F, -2.0F, 4, 6, 4, 0.125F, false));
		emitter.cubeList.add(new ModelBox(emitter, 28, 4, -2.0F, -16.25F, -2.0F, 4, 1, 4, 0.0F, false));
		emitter.cubeList.add(new ModelBox(emitter, 21, 25, -1.0F, -15.875F, 1.375F, 2, 2, 1, 0.0F, false));

		vents = new RendererModel(this);
		vents.setRotationPoint(3.0F, 0.0F, 0.5F);
		emitter.addChild(vents);
		vents.cubeList.add(new ModelBox(vents, 0, 0, -8.0F, -15.5F, -2.0F, 10, 3, 3, 0.0F, false));

		ventw = new RendererModel(this);
		ventw.setRotationPoint(0.0F, 0.0F, 0.0F);
		vents.addChild(ventw);
		ventw.cubeList.add(new ModelBox(ventw, 0, 0, 2.25F, -12.75F, -1.5F, 0, 0, 2, 0.25F, false));
		ventw.cubeList.add(new ModelBox(ventw, 0, 0, 2.5F, -12.75F, -1.5F, 0, 0, 2, 0.25F, false));
		ventw.cubeList.add(new ModelBox(ventw, 0, 0, 2.25F, -13.75F, -1.75F, 0, 1, 0, 0.25F, false));
		ventw.cubeList.add(new ModelBox(ventw, 0, 0, 2.25F, -13.75F, 0.75F, 0, 1, 0, 0.25F, false));

		vente = new RendererModel(this);
		vente.setRotationPoint(-6.0F, 0.0F, 0.0F);
		vents.addChild(vente);
		vente.cubeList.add(new ModelBox(vente, 0, 0, -2.25F, -12.75F, -1.5F, 0, 0, 2, 0.25F, false));
		vente.cubeList.add(new ModelBox(vente, 0, 0, -2.5F, -12.75F, -1.5F, 0, 0, 2, 0.25F, false));
		vente.cubeList.add(new ModelBox(vente, 0, 0, -2.25F, -13.75F, -1.75F, 0, 1, 0, 0.25F, false));
		vente.cubeList.add(new ModelBox(vente, 0, 0, -2.25F, -13.75F, 0.75F, 0, 1, 0, 0.25F, false));

		button = new RendererModel(this);
		button.setRotationPoint(0.0F, 0.0F, 0.0F);
		emitter.addChild(button);
		button.cubeList.add(new ModelBox(button, 0, 19, -1.0F, -13.0F, -2.5F, 2, 3, 1, 0.0F, false));
		button.cubeList.add(new ModelBox(button, 23, 0, -1.0F, -11.5F, -2.75F, 2, 2, 1, -0.125F, false));
	}
	@Override
	public void renderHandle(float f5) {
		bindTex();
		handle.render(f5);
	}
	@Override
	public void renderEmitter(float f5) {
		bindTex();
		emitter.render(f5);
	}
	@Override
	public void renderPommel(float f5) {
		bindTex();
		endcap.render(f5);
	}
	@Override
	public void render(float f5) {
		bindTex();
		lightsaber.render(f5);
	}
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
	@Override
	public void bindTex() {
		mc.getTextureManager().bindTexture(TEX);
	}

	@Override
	public float getHeight() {
		return 9.25f;
	}

	@Override
	public float getEmitterHeight() {
		return 7.125f;
	}

	@Override
	public float getCrossGuardOffset() {
		return 2.25f;
	}
}