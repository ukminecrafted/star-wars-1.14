package com.honeyluck.starwars.client.gui;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.common.container.AbstractSaberConstructorContainer;
import com.honeyluck.starwars.common.container.ContainerComponentConstructor;
import com.honeyluck.starwars.common.recipe.ComponentRecipe;
import com.honeyluck.starwars.common.recipe.RecipeRegistries;
import com.honeyluck.starwars.common.tileentity.TileEntityComponentConstructor;
import com.honeyluck.starwars.events.Registries;
import com.honeyluck.starwars.network.MessageCraftComponent;
import com.honeyluck.starwars.network.MessageUpdateSaberConstructor;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import net.minecraftforge.fml.client.config.GuiUtils;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import java.util.List;

public class ComponentConstructorScreen extends ContainerScreen<ContainerComponentConstructor> {

    private ResourceLocation TEXTURE = new ResourceLocation(StarWars.modid, "textures/gui/componentconstructor.png");
    private int bx = 80;
    private int by = 8;
    private int x;
    private int y;

    public ComponentConstructorScreen(ContainerComponentConstructor container, PlayerInventory inv, ITextComponent titleIn) {
        super(container, inv, titleIn);
        this.xSize = 176;
        this.ySize = 166;
    }

    @Override
    protected void init() {
        super.init();
        this.x = (this.width - xSize) / 2;
        this.y = (this.height - ySize) / 2;

    }

    @Override
    public void tick() {
        super.tick();
        this.bx = 0;
        this.by = 0;

        for(Widget button : buttons) {
            button.active = false;
        }

        this.buttons.clear();

        for (ComponentRecipe recipe : RecipeRegistries.componentRecipes) {
            ItemStackHandler inventory = ((TileEntityComponentConstructor)this.getContainer().getTileEntity()).getInventory();
            if(recipe.matches(inventory.getStackInSlot(0), inventory.getStackInSlot(1), inventory.getStackInSlot(2), inventory.getStackInSlot(3))) {
                ItemStack output = new ItemStack(recipe.getOutput());
                ComponentConstructorButton btn = new ComponentConstructorButton(x + 82 + bx, y + 9 + by, 16, 16, output, (button) -> {
                    StarWars.NETWORK_CHANNEL.send(PacketDistributor.SERVER.noArg(), new MessageCraftComponent(output));
                });
                this.addButton(btn);
                bx += 17;
                if(bx > 54) {
                    bx = 0;
                    by += 17;
                }
            }
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1, 1, 1, 1);
        this.minecraft.getTextureManager().bindTexture(TEXTURE);

        int xPos = (this.width - this.xSize) / 2;
        int yPos = (this.height - this.ySize) / 2;
        this.blit(xPos, yPos, 0, 0, this.xSize, this.ySize);
        GuiUtils.drawTexturedModalRect(xPos + 156, yPos + 8, 244, 0, 12, 15, 0);

    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground();
        super.render(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }
}
