package com.honeyluck.starwars.client.gui;

import com.honeyluck.starwars.StarWars;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import net.minecraftforge.fml.client.config.GuiUtils;

public class ComponentConstructorButton extends GuiButtonExt {

    private ItemStack itemStack;
    private Minecraft minecraft;
    private ResourceLocation TEXTURE = new ResourceLocation(StarWars.modid, "textures/gui/componentconstructor.png");

    public ComponentConstructorButton(int xPos, int yPos, int width, int height, ItemStack stack, IPressable handler) {
        super(xPos, yPos, width, height, "", handler);
        this.itemStack = stack;
        this.minecraft = Minecraft.getInstance();
    }

    @Override
    public void renderButton(int mouseX, int mouseY, float partial) {

        minecraft.getTextureManager().bindTexture(TEXTURE);
        GuiUtils.drawTexturedModalRect(x,y, !isHovered() ? 0 : 17, 167, 16, 16, 0);
        GlStateManager.pushMatrix();
        RenderHelper.enableGUIStandardItemLighting();
        GlStateManager.enableDepthTest();
        minecraft.getItemRenderer().renderItemIntoGUI(itemStack, x, y);
        GlStateManager.popMatrix();
    }

    public ItemStack getItemStack() {
        return itemStack;
    }
}
