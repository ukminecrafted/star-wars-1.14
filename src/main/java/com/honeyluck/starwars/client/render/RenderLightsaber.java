package com.honeyluck.starwars.client.render;

import com.honeyluck.starwars.client.models.sabers.*;
import com.honeyluck.starwars.common.item.EnumSaberParts;
import com.honeyluck.starwars.common.item.ItemKyberCrystal;
import com.honeyluck.starwars.common.item.ItemSaber;
import com.honeyluck.starwars.common.item.ItemSaberPart;
import com.honeyluck.starwars.common.utils.RenderUtils;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.ItemStackTileEntityRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.items.CapabilityItemHandler;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.HashMap;

@OnlyIn(Dist.CLIENT)
public class RenderLightsaber extends ItemStackTileEntityRenderer {

    private static ModelSaber saber;
    private static ItemStack crystal;
    private static ItemSaberPart pommel = null;
    private static ItemSaberPart emitter = null;
    private float scale = 0.4f;


    @Override
    public void renderByItem(ItemStack itemStackIn) {
        int renderPass = ForgeHooksClient.getWorldRenderPass();
        ItemSaber item = (ItemSaber) itemStackIn.getItem();
        emitter = null;
        pommel = null;

        itemStackIn.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(a -> {
            if(a.getStackInSlot(0).getItem() instanceof ItemSaberPart) {
                emitter = (ItemSaberPart) a.getStackInSlot(0).getItem();
            }
            crystal = a.getStackInSlot(1);
            if(a.getStackInSlot(2).getItem() instanceof ItemSaberPart) {
                pommel = (ItemSaberPart) a.getStackInSlot(2).getItem();
            }
        });

        saber = RenderUtils.saberModels.get(item.getModel());

        GlStateManager.pushMatrix();

        // +x moves right, +y moves forward, +z moves up
        GlStateManager.translated(8*0.0625, 4.5*0.0625, 8*0.0625f);

        //Render Lightsaber
            GlStateManager.pushMatrix();
            GlStateManager.rotatef(180, 0, 0, 1);
            GlStateManager.rotatef(180, 0, 1, 0);
            GlStateManager.scaled(scale, scale, scale);
            if(pommel != null)RenderUtils.saberModels.get(pommel.getModel()).renderPommel(0.0625f);
            saber.renderHandle(0.0625f);

            if(emitter != null) {
                float offset = RenderUtils.saberModels.get(emitter.getModel()).getHeight() * 0.0625f - saber.getHeight() * 0.0625f;
                GlStateManager.translatef(0, offset, 0);//Offsets based on difference between saber heights
                RenderUtils.saberModels.get(emitter.getModel()).renderEmitter(0.0625f);
                GlStateManager.translatef(0, -offset, 0); //Reset position to base of the handle (model 0,0,0)
            }
            GlStateManager.popMatrix();

        if(itemStackIn.hasTag() && renderPass == 0) {
            float extension = itemStackIn.getTag().getFloat("prevSaberExtension") + (itemStackIn.getTag().getFloat("saberExtension") - itemStackIn.getTag().getFloat("prevSaberExtension")) * RenderUtils.renderTickTime;
            float crossExtension = itemStackIn.getTag().getFloat("prevCrossExtension") + (itemStackIn.getTag().getFloat("crossExtension") - itemStackIn.getTag().getFloat("prevCrossExtension")) * RenderUtils.renderTickTime;

            if (extension > 0F) {
                if(crystal.getItem() instanceof ItemKyberCrystal) {
                    int red = crystal.getTag().getInt("red");
                    int green = crystal.getTag().getInt("green");
                    int blue = crystal.getTag().getInt("blue");

                    double offset = saber.getHeight() + RenderUtils.saberModels.get(emitter.getModel()).getEmitterHeight(); //Move blade from (0,0,0) to emitter
                    double crossGuardOffset = RenderUtils.saberModels.get(emitter.getModel()).getCrossGuardOffset();

	                GlStateManager.translated(0, offset * scale * 0.0625, 0); //Position Blade

                    GlStateManager.disableTexture(); // seems to help to render the faces at some camera angles *insert shrug*
                    GlStateManager.disableLighting();
                    GlStateManager.disableCull(); // seems to help to render the faces at most camera angles *insert shrug*
                    GlStateManager.enableBlend();
                    RenderUtils.setLightmapTextureCoords(240, 240);

                    renderBlade(new Color(red, green, blue), extension, 1.2f * scale);

                    if(emitter.getPartType() == EnumSaberParts.CROSSGUARD) {
                        GlStateManager.translated(0, -crossGuardOffset * scale * 0.0625, 0); //Position crossblades
                        renderCrossGuard(new Color(red, green, blue), crossExtension, 0.5f * scale);

                    }

                    RenderUtils.restoreLightmapTextureCoords();
                    GlStateManager.disableBlend();
                    GlStateManager.enableCull();
                    GlStateManager.enableLighting();
                    GlStateManager.enableTexture();

                }
            }
        }
        GlStateManager.popMatrix();
    }

    private void renderBlade(Color color, float extension, float radius) {
        GlStateManager.pushMatrix();

        {
            float width = radius/2 * 0.0625f; // 0.5 is half width, actual width spans both directions
            float height = extension;
            drawSaber(width, height, color, true, true, true);

            // Render White part
            GlStateManager.color3f(1, 1, 1);
            GlStateManager.depthMask(true);
            drawLine(width, height, true, true);

        }

        GlStateManager.popMatrix();
    }

    private void renderCrossGuard(Color color, float extension, float radius) {
        GlStateManager.pushMatrix();
        float width = radius / 2 * 0.0625f; // 0.5 is half width, actual width spans both directions
        GlStateManager.rotatef(90, 0, 0, 1);
        drawSaber(width, extension, color, true, true, true);
        GlStateManager.rotatef(180, 0, 0, 1);
        drawSaber(width, extension, color, true, true, true);

        // Render White part
        GlStateManager.color3f(1, 1, 1);
        GlStateManager.depthMask(true);

        drawLine(width, extension, true, true);
        GlStateManager.rotatef(180, 0, 0, 1);
        drawLine(width, extension, true, true);

        GlStateManager.popMatrix();

    }

    public void drawSaber(float width, float height, Color color, boolean extendedEnd, boolean drawTop, boolean drawBottom) {
        for (int i = 1; i < 3; ++i) {
            GlStateManager.color4f(color.getRed() / 255f, color.getGreen() / 255f, color.getBlue() / 255f, 1f / i / 2);
            GlStateManager.depthMask(false);

            float growWidth = width + i * 0.2f * 0.0625f;
            float growHeight = extendedEnd ? height + i * 0.5f * 0.0625f : height;

            drawLine(growWidth, growHeight, drawTop, drawBottom);
        }
    }

    public void drawLine(float width, float length, boolean drawTop, boolean drawBottom) {
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bb = tessellator.getBuffer();
        bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION); // Draws in quads format, and accepts position only

        // West Side
        bb.pos(width, 0, -width).endVertex();
        bb.pos(width, length, -width).endVertex();
        bb.pos(width, length, width).endVertex();
        bb.pos(width, 0, width).endVertex();

        // East Side
        bb.pos(-width, 0, -width).endVertex();
        bb.pos(-width, length, -width).endVertex();
        bb.pos(-width, length, width).endVertex();
        bb.pos(-width, 0, width).endVertex();

        // South
        bb.pos(-width, 0, width).endVertex();
        bb.pos(-width, length, width).endVertex();
        bb.pos(width, length, width).endVertex();
        bb.pos(width, 0, width).endVertex();

        // North
        bb.pos(-width, 0, -width).endVertex();
        bb.pos(-width, length, -width).endVertex();
        bb.pos(width, length, -width).endVertex();
        bb.pos(width, 0, -width).endVertex();

        if (drawTop) {
            // Up
            bb.pos(-width, length, -width).endVertex();
            bb.pos(width, length, -width).endVertex();
            bb.pos(width, length, width).endVertex();
            bb.pos(-width, length, width).endVertex();
        }

        if (drawBottom) {
            // Down
            bb.pos(-width, 0, -width).endVertex();
            bb.pos(width, 0, -width).endVertex();
            bb.pos(width, 0, width).endVertex();
            bb.pos(-width, 0, width).endVertex();
        }

        tessellator.draw();
    }
}
