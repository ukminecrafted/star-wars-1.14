package com.honeyluck.starwars.client.render;

import com.honeyluck.starwars.client.models.sabers.ModelSaber;
import com.honeyluck.starwars.common.item.EnumSaberParts;
import com.honeyluck.starwars.common.item.ItemSaberPart;
import com.honeyluck.starwars.common.utils.RenderUtils;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.tileentity.ItemStackTileEntityRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class RenderSaberPart extends ItemStackTileEntityRenderer {

    private ModelSaber model;
    @Override
    public void renderByItem(ItemStack stack) {
        ItemSaberPart item = (ItemSaberPart) stack.getItem();
        model = RenderUtils.saberModels.get(item.getModel());
        if(item.getPartType() == EnumSaberParts.POMMEL)
            GlStateManager.translated(8.25*0.0625, 8.5*0.0625, 8.5*0.0625);
        else
            GlStateManager.translated(8.25*0.0625, 0.0625, 8.5*0.0625);
        GlStateManager.rotatef(180, 0, 0, 1);
        GlStateManager.rotatef(180, 0, 1, 0);
        GlStateManager.scaled(0.4, 0.4, 0.4);
        switch(item.getPartType()) {
            case EMITTER:
            case CROSSGUARD:
                model.renderEmitter(0.0625f);
                break;
            case HANDLE:
                model.renderHandle(0.0625f);
                break;
            case POMMEL:
                model.renderPommel(0.0625f);
                break;
        }
    }
}
