package com.honeyluck.starwars.client.render.entity;

import com.honeyluck.starwars.common.entity.EntityHolocron;
import com.honeyluck.starwars.common.utils.RenderUtils;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;
import java.util.Random;

public class RenderEntityHolocron extends EntityRenderer<EntityHolocron> {


    public RenderEntityHolocron(EntityRendererManager renderManager) {
        super(renderManager);
    }

    @Nullable
    @Override
    protected ResourceLocation getEntityTexture(EntityHolocron entity) {
        return null;
    }

    @Override
    public void doRender(EntityHolocron entity, double x, double y, double z, float entityYaw, float partialTicks) {

        Random random = new Random(432L);
        Minecraft minecraft = Minecraft.getInstance();
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();

        float f = random.nextFloat();
        float f1 = 0.0F;
        if (f > 0.8F) {
            f1 = (f - 0.8F) / 0.2F;
        }

        GlStateManager.pushMatrix();
        GlStateManager.translated(x, y + 0.25f, z);
        GlStateManager.rotatef(45, 0, 1, 0);
        GlStateManager.rotatef(minecraft.world.getGameTime() + partialTicks, 0, 1, 0);
        minecraft.getItemRenderer().renderItem(entity.getItem(), ItemCameraTransforms.TransformType.GROUND);
        RenderHelper.disableStandardItemLighting();
        GlStateManager.disableTexture();
        GlStateManager.shadeModel(7425);
        GlStateManager.enableBlend();
        GlStateManager.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE);
        GlStateManager.disableAlphaTest();
        GlStateManager.enableCull();
        GlStateManager.depthMask(false);
        GlStateManager.translated(0, -0.1, 0);
        for(int i = 0; (float)i < (f + f * f) / 2.0F * 30.0F; ++i) {
            GlStateManager.rotatef(random.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
            GlStateManager.rotatef(random.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
            GlStateManager.rotatef(random.nextFloat() * 360.0F, 0.0F, 0.0F, 1.0F);
            GlStateManager.rotatef(random.nextFloat() * 360.0F, 1.0F, 0.0F, 0.0F);
            GlStateManager.rotatef(random.nextFloat() * 360.0F, 0.0F, 1.0F, 0.0F);
            GlStateManager.rotatef(random.nextFloat() * 360.0F + f * 90.0F, 0.0F, 0.0F, 1.0F);
            float f2 = random.nextFloat() * 2.0F + 5.0F + f1 * 10.0F;
            float f3 = random.nextFloat() * 2.0F + 1.0F + f1 * 2.0F;
            bufferbuilder.begin(6, DefaultVertexFormats.POSITION_COLOR);
            bufferbuilder.pos(0.0D, 0.0D, 0.0D).color(70, 140, 180, (int)(100.0F * (1.0F - f1))).endVertex();
            bufferbuilder.pos(-0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(70, 140, 180, 0).endVertex();
            bufferbuilder.pos(0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(70, 140, 180, 0).endVertex();
            bufferbuilder.pos(0.0D, (double)f2, (double)(1.0F * f3)).color(70, 140, 180, 0).endVertex();
            bufferbuilder.pos(-0.866D * (double)f3, (double)f2, (double)(-0.5F * f3)).color(70, 140, 180, 0).endVertex();
            tessellator.draw();
        }

        GlStateManager.depthMask(true);
        GlStateManager.disableCull();
        GlStateManager.disableBlend();
        GlStateManager.shadeModel(7424);
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.enableTexture();
        GlStateManager.enableAlphaTest();
        RenderHelper.enableStandardItemLighting();

        GlStateManager.popMatrix();

    }
}
