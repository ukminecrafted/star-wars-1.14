package com.honeyluck.starwars.client.render;

import com.honeyluck.starwars.client.gui.SaberConstructorScreen;
import com.honeyluck.starwars.common.tileentity.TileEntitySaberConstructor;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * Created by Nictogen on 2020-04-07.
 */
@OnlyIn(Dist.CLIENT)
public class RenderSaberConstructor extends TileEntityRenderer<TileEntitySaberConstructor>
{
	@Override public void render(TileEntitySaberConstructor tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage)
	{
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y, z);
		switch (tileEntityIn.getWorld().getBlockState(tileEntityIn.getPos()).get(BlockStateProperties.HORIZONTAL_FACING))
		{
		case DOWN:
		case UP:
		case SOUTH:
			break;
		case NORTH:
			GlStateManager.rotatef(180f, 0, 1f, 0);
			GlStateManager.translatef(-1f, 0f, -1f);
			break;
		case WEST:
			GlStateManager.rotatef(-90f, 0, 1f, 0);
			GlStateManager.translatef(0f, 0f, -1f);
			break;
		case EAST:
			GlStateManager.rotatef(90f, 0, 1f, 0);
			GlStateManager.translatef(-1f, 0f, 0f);
			break;
		}

		//Emitter
		GlStateManager.pushMatrix();
		GlStateManager.translated(-0.5, 1.1, 0.3);
		if(tileEntityIn.getSaberConstructorMode() == SaberConstructorScreen.SaberConstructorMode.MODIFY)
		Minecraft.getInstance().getItemRenderer().renderItem(tileEntityIn.getInventory().getStackInSlot(19), ItemCameraTransforms.TransformType.GROUND);
		GlStateManager.popMatrix();

		//Crystal
		GlStateManager.pushMatrix();
		GlStateManager.translated(-0.2, 1.175, 0.315);
		GlStateManager.rotatef(90f, 0.0f, 0.0f, 1.0f);
		GlStateManager.scalef(0.25f, 0.25f, 0.25f);
		if(tileEntityIn.getSaberConstructorMode() == SaberConstructorScreen.SaberConstructorMode.MODIFY)
		Minecraft.getInstance().getItemRenderer().renderItem(tileEntityIn.getInventory().getStackInSlot(20), ItemCameraTransforms.TransformType.GROUND);
		GlStateManager.popMatrix();

		//Handle
		GlStateManager.pushMatrix();
		if(tileEntityIn.getSaberConstructorMode() == SaberConstructorScreen.SaberConstructorMode.MODIFY) {
			GlStateManager.translated(0.1, 1.1, 0.325);
		} else {
			GlStateManager.translated(0, 1.1, 0.325);
		}

		GlStateManager.rotatef(180f, 1.0f, 0.0f, 0.0f);
		Minecraft.getInstance().getItemRenderer().renderItem(tileEntityIn.getInventory().getStackInSlot(18), ItemCameraTransforms.TransformType.GROUND);
		GlStateManager.popMatrix();

		//Endcap
		GlStateManager.pushMatrix();
		GlStateManager.translated(0.4, 1.1, 0.3);
		if(tileEntityIn.getSaberConstructorMode() == SaberConstructorScreen.SaberConstructorMode.MODIFY)
		Minecraft.getInstance().getItemRenderer().renderItem(tileEntityIn.getInventory().getStackInSlot(21), ItemCameraTransforms.TransformType.GROUND);
		GlStateManager.popMatrix();
		GlStateManager.popMatrix();

	}
}
