package com.honeyluck.starwars.network;

import com.honeyluck.starwars.client.gui.SaberConstructorScreen;
import com.honeyluck.starwars.common.container.ContainerComponentConstructor;
import com.honeyluck.starwars.common.tileentity.TileEntityComponentConstructor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class MessageCraftComponent {

    public ItemStack itemStack;

    public MessageCraftComponent(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public MessageCraftComponent(PacketBuffer buffer) {
        this.itemStack = buffer.readItemStack();
    }

    public void toBytes(PacketBuffer buffer) {
        buffer.writeItemStack(this.itemStack);
    }
    public void handle(Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> {
            PlayerEntity player = ctx.get().getSender();
            if(player != null) {
                if(player.openContainer instanceof ContainerComponentConstructor) {
                    ContainerComponentConstructor container = (ContainerComponentConstructor) player.openContainer;
                    if(container.getTileEntity() instanceof TileEntityComponentConstructor) {
                        TileEntityComponentConstructor te = (TileEntityComponentConstructor)container.getTileEntity();
                        te.getInventory().setStackInSlot(4, itemStack);
                    }
                }
            }
        });
        ctx.get().setPacketHandled(true);
    }
}
