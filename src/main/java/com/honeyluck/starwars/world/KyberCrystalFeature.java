package com.honeyluck.starwars.world;

import com.honeyluck.starwars.events.Registries;
import com.mojang.datafixers.Dynamic;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.ProbabilityConfig;

import java.util.Random;
import java.util.function.Function;

public class KyberCrystalFeature extends Feature<ProbabilityConfig> {


    public KyberCrystalFeature(Function<Dynamic<?>, ? extends ProbabilityConfig> configFactoryIn) {
        super(configFactoryIn);
    }

    @Override
    public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, ProbabilityConfig config) {
        BlockPos newPos = pos;
        boolean generated = false;
        for(int y = newPos.getY(); y > 0; y--) {
            newPos = new BlockPos(newPos.getX(), y, newPos.getZ());
            if((worldIn.getBlockState(newPos).isAir(worldIn, newPos) && worldIn.getBlockState(newPos.down()).isSolid())) { //Rewrite this eventually for different facing crystals
                worldIn.setBlockState(newPos, Registries.kyberCrystals.getDefaultState(), 2);
                generated = true;
                break;
            }
        }

        return generated;
    }
}
