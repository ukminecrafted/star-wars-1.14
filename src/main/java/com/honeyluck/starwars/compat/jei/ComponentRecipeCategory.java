package com.honeyluck.starwars.compat.jei;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.common.recipe.ComponentRecipe;
import com.honeyluck.starwars.events.Registries;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;

import java.util.ArrayList;
import java.util.List;

public class ComponentRecipeCategory implements IRecipeCategory<ComponentRecipe> {

    public static final ResourceLocation name = new ResourceLocation(StarWars.modid + "component_crafting");
    private IDrawable icon;
    private IDrawable background;

    public ComponentRecipeCategory(IGuiHelper gui) {
        background = gui.createDrawable(new ResourceLocation(StarWars.modid, "textures/gui/componentconstructor.png"), 5, 5, 67, 40);
        icon = gui.createDrawableIngredient(new ItemStack(Registries.saberConstructor));
    }

    @Override
    public ResourceLocation getUid() {
        return name;
    }

    @Override
    public Class<? extends ComponentRecipe> getRecipeClass() {
        return ComponentRecipe.class;
    }

    @Override
    public String getTitle() {
        return "Lightsaber Component Constructing";
    }

    @Override
    public IDrawable getBackground() {
        return background;
    }

    @Override
    public IDrawable getIcon() {
        return icon;
    }

    @Override
    public void setIngredients(ComponentRecipe componentRecipe, IIngredients ingredients) {
        List<ItemStack> ins = new ArrayList<ItemStack>();
        for(Item it : componentRecipe.getInputs()) {
            ins.add(new ItemStack(it));
        }
        ingredients.setInputs(VanillaTypes.ITEM, ins);
        ingredients.setOutput(VanillaTypes.ITEM, new ItemStack(componentRecipe.getOutput()));
    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, ComponentRecipe componentRecipe, IIngredients ingredients) {
        ItemStack[] stacks = new ItemStack[5];
        for(int i = 0; i < stacks.length; i++) {
            if(i < componentRecipe.getInputs().size()) {
                stacks[i] = new ItemStack(componentRecipe.getInputs().get(i));
            } else {
                stacks[i] = ItemStack.EMPTY;
            }
        }

        //Inputs -> slot ID, not sure about the boolean, x, y
        addSlot(recipeLayout, 0, true, 2,2, stacks[0]);
        addSlot(recipeLayout, 1, true, 20, 2, stacks[1]);
        addSlot(recipeLayout, 2, true, 2, 20, stacks[2]);
        addSlot(recipeLayout, 3, true, 20, 20, stacks[3]);

        //Output
        addSlot(recipeLayout, 4, false, 47, 11, new ItemStack(componentRecipe.getOutput()));

    }

    public static void addSlot(IRecipeLayout layout, int id, boolean input, int x, int y, ItemStack stack) {
        if(!stack.isEmpty()) {
            layout.getItemStacks().init(id, input, x, y);
            layout.getItemStacks().set(id, stack);
        }
    }
}
