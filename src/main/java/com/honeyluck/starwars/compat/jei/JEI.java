package com.honeyluck.starwars.compat.jei;

import com.honeyluck.starwars.StarWars;
import com.honeyluck.starwars.common.recipe.RecipeRegistries;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.registration.IRecipeCategoryRegistration;
import mezz.jei.api.registration.IRecipeRegistration;
import net.minecraft.util.ResourceLocation;

@JeiPlugin
public class JEI implements IModPlugin {

    public static final ResourceLocation name = new ResourceLocation(StarWars.modid + "jei");

    @Override
    public ResourceLocation getPluginUid() {
        return name;
    }


    @Override
    public void registerCategories(IRecipeCategoryRegistration registration) {
        registration.addRecipeCategories(new ComponentRecipeCategory(registration.getJeiHelpers().getGuiHelper()));
    }

    @Override
    public void registerRecipes(IRecipeRegistration registration) {
        registration.addRecipes(RecipeRegistries.componentRecipes, ComponentRecipeCategory.name);
    }
}
